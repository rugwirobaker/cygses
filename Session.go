package cygses

//Session ...
type Session sesTable

//Get returns specified session value given the key(string).
func (ses Session) Get(s string) string {
	sessionBin.mux.Lock()
	defer sessionBin.mux.Unlock()
	return ses.valTable[s]
}

//Set sets a session value(string) given its key(string)
func (ses Session) Set(string, value string) {
	sessionBin.mux.Lock()
	defer sessionBin.mux.Unlock()
	ses.valTable[string] = value
}

//Kill kills a session i.e sets the value of ses.dead true.
func (ses Session) Kill(s string) {
	sessionBin.mux.Lock()
	defer sessionBin.mux.Unlock()
	ses.dead = true
}
